# Zenith

```
                         ______          _ _   _
                        |___  /         (_) | | |
                           / / ___ _ __  _| |_| |__
                          / / / _ \ '_ \| | __| '_ \
                         / /_|  __/ | | | | |_| | | |
                        /_____\___|_| |_|_|\__|_| |_|
                             Zcash Full Node CLI
```

Zenith is a command-line interface for the Zcash Full Node (`zcashd`). It has the following features:

- Listing transparent and shielded addresses and balances known to the node, including viewing-only.
- Listing transactions for specific addresses, decoding memos for easy reading.
- Copying addresses to the clipboard.
- Creating new Unified Addresses.
- Sending transactions with shielded memo support.

Note: Zenith depends on a patched version of the `haskoin-core` Haskell package included in this repo. A pull request to the maintainers of `haskoin-core` has been submitted, if/when it is merged, Zenith will be updated to use the standard package.

## Installation

- Clone the repository.
- Install dependencies:
	- [Stack](https://docs.haskellstack.org/en/stable/README/#how-to-install)
	- [Zcash Full Node v.5.0.0](https://zcash.readthedocs.io/en/latest/rtd_pages/zcashd.html#install)
	- `xclip`
	- `libsecp256k1-dev`
	- `libxss-dev`
- Install using `stack`:

```
stack install
```

## Configuration

- Copy the sample `zenith.cfg` file to a location of your choice and update the values of the user and password for the `zcashd` node. These values can be found in the `zcash.conf` file for the Zcash node.

## Usage

From the location where the configured `zenith.cfg` file is placed, use `zenith` to start.

Zenith will attempt to connect to the node and check compatibility. Connections to `zcashd` versions less than 5.0.0 will fail.

### Available commands

- `?`: Lists available commands.
- `list`: Lists all transparent and shielded addresses and their balance.
  - Notes about balances:
    - Addresses from an imported viewing key will list a balance but it may be inaccurate, as viewing keys cannot see ZEC spent out of that address.
	- Balances for Unified Addresses *belonging to the same account* are shared. Zenith will list the full account balances for each of the UAs in the account.
- `txs <id>`: Lists all transactions belonging to the address corresponding to the `id` given, in chronological order.
- `copy`: Copies the selected address to the clipboard.
- `new`: Prompts the user for the option to include a transparent receiver, a Sapling receiver or both. An Orchard receiver is always included.
- `send`: Prompts the user to prepare an outgoing transaction, selecting the source address, validating the destination address, the amount and the memo. 
  - If the source is a transparent address, the privacy policy is set to `AllowRevealedSenders`, favoring the shielding of funds when sent to a UA.
  - If the source is a shielded address, the privacy policy is set to `AllowRevealedAmounts`, favoring the move of funds from legacy shielded pools to Orchard.
- `uri`: Prompts the user to select the source account and to enter a [ZIP-321](https://zips.z.cash/zip-0321) compliant URI to generate and send a transaction.
- `exit`: Ends the session.

### Support

If you would like to support the development of Zenith, please visit our [Free2Z](https://free2z.com/zenith-full-node-cli) page.
