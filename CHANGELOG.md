# Changelog 

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed

- Sending transactions to transparent addresses, removing the memo and using the `AllowRevealedRecipients` privacy policy.

## [0.3.1.0]

### Added

- Added option to include Reply-To address when using URIs to generate transaction

### Changed

- Improved `encodeHexText` to handle Unicode characters correctly.

## [0.3.0.0]

### Changed

- Changed decoding of memos to support Unicode (emojis!)

### Fixed

- Displaying transactions for view-only addresses

## [0.2.0.0]

### Added

- Adds `uri` CLI command to send funds using a [ZIP-321](https://zips.z.cash/zip-0321) URI
- Adds `sendWithUri` function to support [ZIP-321 URIs](https://zips.z.cash/zip-0321)
- Adds option to include reply-to address in `send` command

### Changed

- Changes the use of `checkOpResult` to be recursive until the transaction either fails or succeeds.

## [0.1.0.0]

### Added

- CHANGELOG.md
- README.md
- List node addresses
- Query an address balance
- List transactions for an address, displaying decoded memos
- Copy address to clipboard
- Create new Unified Addresses
- Sending transactions

